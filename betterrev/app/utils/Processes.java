package utils;

import play.Logger;

import java.io.*;
import java.util.Arrays;

public final class Processes {

    private static final String SHELL = "/bin/sh";

    private Processes() {
        //  Hide Utility Class Constructor - Utility classes should not have a public or default constructor.
    }

    private static class StreamLogger extends Thread {

        private final String prefix;
        private final InputStream input;

        private StreamLogger(String prefix, InputStream input) {
            this.prefix = prefix;
            this.input = input;
        }

        @Override
        public void run() {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(input))) {
                String line;
                while((line = reader.readLine()) != null) {
                    Logger.info(prefix + " > " + line);
                }
            } catch (IOException e) {
                Logger.error(e.getMessage(), e);
            }
        }
    }

    public static int runProcess(String workingDirectory, String... command) throws IOException {
        ProcessBuilder builder = new ProcessBuilder(command);
        builder.directory(new File(workingDirectory));

        Process process = builder.start();

        StreamLogger outputLogger = new StreamLogger("out", process.getInputStream());
        outputLogger.start();

        StreamLogger errorLogger = new StreamLogger("err", process.getErrorStream());
        errorLogger.start();

        try {
            int exitCode = process.waitFor();

            if (exitCode != 0) {
                Logger.error("Unknown exit code when running " + Arrays.toString(command) + " value is " + exitCode);
            }

            return exitCode;
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static int runThroughShell(String workingDirectory, String... args) throws IOException {
        String[] command = new String[args.length + 1];
        command[0] = SHELL;
        System.arraycopy(args, 0, command, 1, args.length);
        return runProcess(workingDirectory, command);
    }

}

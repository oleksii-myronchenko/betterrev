package models;

/**
 * State of Contribution.
 */
public enum State {
    NULL,
    OPEN,
    PENDING_PEER_REVIEW,
    PENDING_MENTOR_APPROVAL,
    ACCEPTED,
    CLOSED,
    COMMITTED
}

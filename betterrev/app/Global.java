import akka.actor.ActorRef;
import akka.actor.Props;

import com.gs.collections.api.block.procedure.Procedure2;
import com.gs.collections.impl.map.mutable.UnifiedMap;

import models.ContributionEvent;
import play.Application;
import play.Logger;
import play.Play;
import play.libs.Akka;
import play.utils.crud.GlobalCRUDSettings;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;
import update.BetterrevActor;
import update.bitbucket.BitbucketPoller;
import update.bitbucket.PollBitbucketEvent;
import update.mercurial.WebrevGenerator;
import update.ocachecker.OCASignedChecker;
import update.ocachecker.OCASignedEvent;
import update.pullrequest.ImportPullRequestsEvent;
import update.pullrequest.PullRequestImporter;

import java.util.concurrent.TimeUnit;

import static com.gs.collections.impl.utility.MapIterate.forEachKeyValue;

/**
 * GlobalCRUDSettings extends Play GlobalSettings
 */
public class Global extends GlobalCRUDSettings {
	
    private final static UnifiedMap<Class<? extends BetterrevActor>, Class<?>> actorToEventMap =
            UnifiedMap.newWithKeysValues(
                    BitbucketPoller.class, PollBitbucketEvent.class,
                    PullRequestImporter.class, ImportPullRequestsEvent.class,
                    WebrevGenerator.class, ContributionEvent.class,
                    OCASignedChecker.class, OCASignedEvent.class
            );

    @Override
    public void onStart(Application app) {
        super.onStart(app);
        if (!app.isTest()) {
            subscribeActorsToEvents();
            regularlyPollBitbucket();
        }
    }

    @SuppressWarnings("serial")
    private static void subscribeActorsToEvents() {
        forEachKeyValue(actorToEventMap, new Procedure2<Class<? extends BetterrevActor>, Class<?>>() {
            @Override
            public void value(Class<? extends BetterrevActor> actorClass, Class<?> eventClass) {
                ActorRef actor = actorOf(actorClass);
                subscribeActorToEvent(actor, eventClass);
            }
        });
    }

    private static ActorRef actorOf(Class<? extends BetterrevActor> actorClass) {
        return Akka.system().actorOf(Props.create(actorClass));
    }

    private static boolean subscribeActorToEvent(ActorRef actor, Class<?> eventClass) {
        return Akka.system().eventStream().subscribe(actor, eventClass);
    }

    private static void regularlyPollBitbucket() {
        long duration = Play.application().configuration().getLong("bitbucket.polling.duration.seconds");
        FiniteDuration pollingDuration = Duration.create(duration, TimeUnit.SECONDS);
        Akka.system().scheduler().schedule(Duration.Zero(), pollingDuration, new Runnable() {
            @Override
            public void run() {
                Logger.debug("Publishing new PollBitbucketEvent message.");
                Akka.system().eventStream().publish(new PollBitbucketEvent());
            }
        }, Akka.system().dispatcher());

    }
}
